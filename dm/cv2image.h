
#pragma once

#include "array_types.h"
#include "image.h"
#include <opencv2/core.hpp>

image_t* Mat2Image(const cv::Mat& gray) {
  CV_Assert(gray.depth() == CV_8U);
  CV_Assert(gray.channels() == 1);

  image_t* I = image_new(gray.cols, gray.rows);

  for (int row = 0; row < gray.rows; row++) {
    for (int col =0; col < gray.cols; col++) {
      I->data[col + row*I->stride] = gray.at<uchar>(row, col);
    }
  }

  return I;
}

cv::Mat Image2Mat(image_t* gray) {

}

