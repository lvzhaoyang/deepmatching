/**
 * @file DeepMatching.h
 * @brief this is a wrapper for deep matching
 * @author Zhaoyang Lv
 */

#pragma once

#include <dm/deep_matching.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <iostream>

class cvDM {
public:
  typedef typename boost::shared_ptr<cvDM> shared_ptr;
  typedef typename boost::shared_ptr<const cvDM> const_shared_ptr;

  struct Parameters {
    typedef boost::shared_ptr<Parameters> shared_ptr;
    typedef boost::shared_ptr<const Parameters> const_shared_ptr;

    dm_params_t dm_p;        ///< default deep matching parameters;
    scalerot_params_t sr_p;  ///< scale and rotation matching parameters

    bool use_scalerot;       ///< true if use scale rotation matching
    float scale_col;
    float scale_row;

    Parameters () {
      set_default_dm_params(&dm_p);
      set_default_scalerot_params(&sr_p);
      // only a small number of parameters are set here
      use_scalerot = false;
      scale_col = 1;
      scale_row = 1;
      dm_p.n_thread = 6;    // override the default thread configuration.
    }
  };

public:
  // use all the default parameters
  cvDM() {
    p_ = boost::make_shared<Parameters>();
  }

  cvDM(const cvDM::Parameters::shared_ptr params)
    : p_(params) {
  }

  std::vector<corres_t> run (const cv::Mat& img1, const cv::Mat& img2) {
    const double start = static_cast<double>(cv::getTickCount());

    CV_Assert(img1.depth() == CV_8U);
    CV_Assert(img2.depth() == CV_8U);
    cv::Mat I1, I2;
    if (img1.channels() == 3) {
      cv::cvtColor(img1, I1, CV_BGR2GRAY);
    } else {
      I1 = img1;
    }
    if (img2.channels() == 3) {
      cv::cvtColor(img2, I2, CV_BGR2GRAY);
    } else {
      I2 = img2;
    }

    image_t* im1 = Mat2Image(I1);
    image_t* im2 = Mat2Image(I2);

    float_image* c_ptr;
    if (p_->use_scalerot) {
      c_ptr = deep_matching_scale_rot(im1, im2, &p_->dm_p, &p_->sr_p);
    } else {
      c_ptr = deep_matching(im1, im2, &p_->dm_p, NULL);
    }

    std::vector<corres_t> corres((corres_t*)c_ptr->pixels,
                                 (corres_t*)c_ptr->pixels + c_ptr->ty);

    if (p_->scale_col != 1 || p_->scale_row != 1) {
      BOOST_FOREACH(corres_t& c, corres) {
        c.x0 *= p_->scale_col;
        c.y0 *= p_->scale_row;
        c.x1 *= p_->scale_col;
        c.y1 *= p_->scale_row;
      }
    }

    free_image(c_ptr);
    image_delete(im1);
    image_delete(im2);

    const double timeSec = (cv::getTickCount() - start)/cv::getTickFrequency();
    std::cout << "Calcualte Deep Matching: " << timeSec << "sec"
              << std::endl;

    return corres;
  }

  static void Draw(const cv::Mat& I1, const cv::Mat& I2,
                   const std::vector<corres_t>& corres) {
//    cv::Mat D(I1.rows, I1.cols, CV_8UC3, cvScalar(0,0,0));
    cv::Mat im1 = I1.clone();
    cv::Mat im2 = I2.clone();
    BOOST_FOREACH(const corres_t& c, corres){
      int sCol, sRow, dCol, dRow;
      sCol = c.x0;
      sRow = c.y0;
      dCol = c.x1;
      dRow = c.y1;

      if (dRow < 0 || dRow >= I2.rows || dCol < 0 || dCol >= I2.cols) continue;

      cv::circle(im1, cv::Point2i(sCol, sRow), c.score*1e-3, cv::Scalar(255,0,0));
      cv::circle(im2, cv::Point2i(dCol, dRow), c.score*1e-3, cv::Scalar(255,0,0));
      cv::line(im1, cv::Point2i(sCol, sRow), cv::Point2i(dCol, dRow), cv::Scalar(0,255,0));
    }

    cv::imshow("I1", im1);
    cv::imshow("I2", im2);
    cv::waitKey(0);
  }

  static void Print(const std::vector<corres_t>& corres) {
    BOOST_FOREACH(const corres_t& c, corres){
      std::cout << " xy in I0: " << c.x0 << " " << c.y0
           << " xy in I1: " << c.x1 << " " << c.y1
           << " maxima: " << c.maxima
           << " weight: " << c.score << std::endl;
    }

  }

private:

  image_t* Mat2Image(const cv::Mat& gray) {
    CV_Assert(gray.depth() == CV_8U);
    CV_Assert(gray.channels() == 1);

    image_t* I = image_new(gray.cols, gray.rows);

    for (int row = 0; row < gray.rows; row++) {
      for (int col =0; col < gray.cols; col++) {
        I->data[col + row*I->stride] = gray.at<uchar>(row, col);
      }
    }

    return I;
  }

  Parameters::shared_ptr p_;
};




