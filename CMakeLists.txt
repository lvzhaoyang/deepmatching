cmake_minimum_required(VERSION 2.6)

project(deepMatching)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")    #enable c++ 11

find_package(CUDA)
list(APPEND CUDA_NVCC_FLAGS "-arch=sm_35;-std=c++11;-O2;-DVERBOSE")

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})
link_directories(${OpenCV_LIBRARY_DIRS})

find_package(Caffe REQUIRED)
include_directories(${Caffe_INCLUDE_DIRS})
add_definitions(${Caffe_DEFINITIONS})

find_package(Boost 1.46 COMPONENTS system program_options filesystem REQUIRED)
message(STATUS "Boost include: " ${Boost_INCLUDE_DIRS})

include_directories(${CMAKE_SOURCE_DIR})
link_directories(${CMAKE_INSTALL_PREFIX}/lib)

file(GLOB CPU_HEADERS "dm/*.hpp" "dm/*.h")
file(GLOB CPU_FILES "dm/*.cpp" "dm/*.c")

file(GLOB GPU_HEADERS "dm/cuda/*.h" "dm/cuda/*.hpp")
file(GLOB GPU_FILES "dm/cuda/*.cpp" "dm/cuda/*.cu" "dm/cuda/*.c")

add_library(deepMatching SHARED ${CPU_HEADERS} ${CPU_FILES})
target_link_libraries(deepMatching png jpeg blas)

cuda_add_library(deepMatchingCu SHARED ${GPU_HEADERS} ${GPU_FILES})
target_link_libraries(deepMatching caffe ${CUDA_cusparse_LIBRARY})

file(GLOB exes "exes/*.cpp")
foreach(exe IN ITEMS ${exes})
  get_filename_component(script_name ${exe} NAME_WE)
  
  # Add executable
  add_executable(${script_name} ${exe})
  target_link_libraries(${script_name}
    ${PROJECT_NAME}
    ${OpenCV_LIBS})
endforeach()

install(FILES ${CPU_HEADERS} DESTINATION include/dm)
install(FILES ${GPU_HEADERS} DESTINATION include/dm/cuda)
install(TARGETS deepMatching DESTINATION lib LIBRARY DESTINATION)
install(TARGETS deepMatchingCu DESTINATION lib LIBRARY DESTINATION)
