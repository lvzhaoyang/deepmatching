/**
 * @author Zhaoyang Lv
 * @brief use opencv wrapper for deep matching
 */

#include <dm/cvDeepMatching.h>

int main() {
  cv::Mat I1 = cv::imread("../testFiles/000000_10.png");
  cv::Mat I2 = cv::imread("../testFiles/000000_11.png");

  cvDM dm;
  std::vector<corres_t> corres = dm.run(I1, I2);
  cvDM::Print(corres);
  cvDM::Draw(I1, I2, corres);
}
