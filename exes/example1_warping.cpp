/**
 * @author Zhaoyang Lv
 * @brief a warping example
 */


#include <dm/cv2image.h>
#include <dm/deep_matching.h>
#include <dm/io.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <fstream>
#include <iostream>

using namespace std;

void showExampleMatching(cv::Mat& I1, cv::Mat& I2, const string& infile);

void cvMatMatching(const cv::Mat& I1, const cv::Mat& I2, const string& outfile);


int main() {
  cv::Mat I1 = cv::imread("../testFiles/000000_10.png");
  cv::Mat I2 = cv::imread("../testFiles/000000_11.png");

  string file("corres.txt");
  cvMatMatching(I1, I2, file);

  // string file("../testFiles/matching.txt");  // the existing file
//  showExampleMatching(I1, I2, file);
}

/// read in opencv images and do the matching
void cvMatMatching(const cv::Mat& I1, const cv::Mat& I2, const string& outfile) {
  cv::Mat Ig1, Ig2;
  cv::cvtColor(I1, Ig1, CV_BGR2GRAY);
  cv::cvtColor(I2, Ig2, CV_BGR2GRAY);

  image_t* im1 = Mat2Image(Ig1);
  image_t* im2 = Mat2Image(Ig2);

  // set params to default
  dm_params_t params;
  set_default_dm_params(&params);
  scalerot_params_t sr_params;
  set_default_scalerot_params(&sr_params);
  bool use_scalerot = false;
  float fx=1, fy=1;   // resizing parameters

  float_image* corres_ptr = deep_matching(im1, im2, &params, NULL );
//  corres_t* corres =  (corres_t*)corres_ptr->pixels;
  vector<corres_t> corres((corres_t*)corres_ptr->pixels,
                          (corres_t*)corres_ptr->pixels + corres_ptr->ty);

  cv::Mat D(I1.rows, I1.cols, CV_8UC3, cvScalar(0,0,0));
  for (int i=0; i < corres.size(); i++) {
    const corres_t& c = corres.at(i); // get one correspodence

    int sCol, sRow, dCol, dRow;
    sCol = c.x0 * fx;
    sRow = c.y0 * fy;
    dCol = c.x1 * fx;
    dRow = c.y1 * fy;

    if (dRow < 0 || dRow >= I2.rows,
        dCol < 0 || dCol >= I2.cols) continue;

    D.at<cv::Vec3b>(dRow, dCol) = I1.at<cv::Vec3b>(sRow, sCol);

    cv::circle(I1, cv::Point2i(sCol, sRow), 1, cv::Scalar(255,0,0));
    cv::circle(I2, cv::Point2i(dCol, dRow), 1, cv::Scalar(255,0,0));
    cv::line(I1, cv::Point2i(sCol, sRow), cv::Point2i(dCol, dRow), cv::Scalar(0,255,0));
  }

  cv::imshow("D", D);
  cv::imshow("I1", I1);
  cv::imshow("I2", I2);
  cv::waitKey(0);

  output_correspondences(outfile.c_str(),
                         (corres_t*)corres_ptr->pixels, corres_ptr->ty, fx, fy );

  free_image(corres_ptr);
  image_delete(im1);
  image_delete(im2);
}

void showExampleMatching(cv::Mat& I1, cv::Mat& I2, const string& infile) {
  cv::Mat D(I1.rows, I1.cols, CV_8UC3);

//  ifstream f("../testFiles/matching.txt");
  ifstream f(infile);
  if (f.is_open()) {
    string line;
    int sCol, sRow, dCol, dRow;
    float maxima, score;
    while (getline(f, line)) {
      istringstream iss(line);
      iss >> sCol >> sRow >> dCol >> dRow >> maxima >> score;
      if (dRow < 0 || dRow >= I2.rows,
          dCol < 0 || dCol >= I2.cols) continue;

      D.at<cv::Vec3b>(dRow, dCol) = I1.at<cv::Vec3b>(sRow, sCol);
      cv::circle(I1, cv::Point2i(sCol, sRow), int(maxima/2), cv::Scalar(255,0,0));
      cv::circle(I2, cv::Point2i(dCol, dRow), int(maxima/2), cv::Scalar(255,0,0));

      // draw the line;
      cv::line (I1, cv::Point2i(sCol, sRow), cv::Point2i(dCol, dRow),
                cv::Scalar(0,255,0));
    }
  }

  f.close();

  cv::imshow("D", D);
  cv::imshow("I1", I1);
  cv::imshow("I2", I2);
  cv::waitKey(0);
}
